﻿function initApp() {
    //  $('#divESPN').load('http://www.espn.com/golf/leaderboard?_ts=' + Date.now());
    $.getJSON("./js/data/getData.php?f=getTournaments&_ts=" + Date.now(), function(data) {
            $('body').data("tournaments", data);
            $.getJSON("./js/data/getData.php?f=getPayments&_ts=" + Date.now(), function(data) {
                    $('body').data("payments", data);
                    $.getJSON("./js/data/getData.php?f=getUsers&_ts=" + Date.now(), function(data) {
                            $('body').data("users", data);
                            initContinue();
                        })
                        .fail(function(data) {
                            console.log(JSON.stringify(data));
                        });
                })
                .fail(function(data) {
                    console.log(JSON.stringify(data));
                });
        })
        .fail(function(data) {
            console.log(JSON.stringify(data));
        });
}


function initContinue() {
    var qstring = location.search.substring(1);
    $('body').data('uid', qstring);

    thisUser = $.grep($('body').data("users"), function(item) {
        return item.Id === $('body').data('uid');
    })[0];

    if (qstring.length > 0 && typeof thisUser !== 'undefined') {
        $('#spanUser')[0].innerText = thisUser.UserName;
        $('body').data('teamName', thisUser.UserName);
        nextTournament = $.grep($('body').data("tournaments"), function(item) {
            return (new Date(item.DtTournament + "T05:00:00Z")) > (new Date());
        })[0];
        todaysTournament = $.grep($('body').data("tournaments"), function(item) {
            return ((new Date(item.DtFinish + "T24:00:00Z")) > (new Date()) && (new Date(item.DtTournament)) < (new Date()));
        })[0];
        if (typeof nextTournament === 'undefined') {
            nextTournament = { Name: 'Too early, come back thursday' }
        } else {
            $('#spanTournamentDates')[0].innerText = $.datepicker.formatDate("M d, yy", new Date(nextTournament.DtTournament + "T05:00:00Z")) +
                ' thru ' + $.datepicker.formatDate("M d, yy", new Date(nextTournament.DtFinish + "T05:00:00Z"));
        }
        $('#spanTournament')[0].innerText = nextTournament.Name;
        $('#divLoading').show();
        $('.divTournament').show();
        $('#divBody').show();
        $('#tabs').show();

        $.getJSON("./js/data/getData.php?f=getPicks&t=" + thisUser.UserName + "&_ts=" + Date.now(), function(data) {
                var alreadyPicked = $.grep(data, function(item) {
                    return item.TournamentName == nextTournament.Name;
                });
                if (alreadyPicked.length == 1) {
                    $('#spanPickedPlayer')[0].innerText = alreadyPicked[0].PlayerName;
                    $('#spanPickedTournament')[0].innerText = nextTournament.Name;
                    $('#divPicked').show();
                    $('#divMakePick').hide();
                } else {
                    $('#divPicked').hide();
                    $('#divMakePick').show();
                }
                $('#divLoading').hide();
                var myhtml = "<p><select id='selectteammenu'></select></p><div id='teamPicks'><table>"
                myhtml += "<thead><tr><th>Player</th><th>Tournament</th><th>Points</th></tr></thead>";
                $.each(data, function(kk, item) {
                    if (item.TeamName !== thisUser.UserName) {
                        if (item.TournamentName !== nextTournament.Name)
                            myhtml += "<tbody><tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=PlayerName>" + item.PlayerName + "</td><td class=TournamentName>" + item.TournamentName + "</td><td class=Points>" + item.Points + "</td></tr>";
                    } else
                        myhtml += "<tbody><tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=PlayerName>" + item.PlayerName + "</td><td class=TournamentName>" + item.TournamentName + "</td><td class=Points>" + item.Points + "</td></tr>";

                });
                myhtml += "</tbody></table></div>";
                $('#pMypicks').html(myhtml);
                // set up previous picks by team name, default to current users team name    
                $.each($('body').data("users"), function(kk, item) {
                    if (item.UserName === thisUser.UserName) {
                        $('#selectteammenu').append('<option selected="selected" value="' + item.UserName + '">' + item.UserName + '</option>');
                    } else {
                        $('#selectteammenu').append('<option value="' + item.UserName + '">' + item.UserName + '</option>');
                    }
                });
                $("#selectteammenu").selectmenu({
                        select: function(event, ui) {
                            var team = $.grep($('body').data("users"), function(item) {
                                return (item.UserName == ui.item.value);
                            })[0];
                            $.getJSON("./js/data/getData.php?f=getPicks&t=" + team.UserName + "&_ts=" + Date.now(), function(data) {
                                var alreadyPicked = $.grep(data, function(item) {
                                    return item.TournamentName == nextTournament.Name;
                                });
                                if (alreadyPicked.length == 1) {
                                    $('#spanPickedPlayer')[0].innerText = alreadyPicked[0].PlayerName;
                                    $('#spanPickedTournament')[0].innerText = nextTournament.Name;
                                    $('#divPicked').show();
                                    $('#divMakePick').hide();
                                } else {
                                    $('#divPicked').hide();
                                    $('#divMakePick').show();
                                }
                                $('#divLoading').hide();
                                var myhtml = "<table>"
                                myhtml += "<thead><tr><th>Player</th><th>Tournament</th><th>Points</th></tr></thead>";
                                $.each(data, function(kk, item) {
                                    if (item.TeamName !== thisUser.UserName) {
                                        if (item.TournamentName !== nextTournament.Name)
                                            myhtml += "<tbody><tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=PlayerName>" + item.PlayerName + "</td><td class=TournamentName>" + item.TournamentName + "</td><td class=Points>" + item.Points + "</td></tr>";
                                    } else
                                        myhtml += "<tbody><tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=PlayerName>" + item.PlayerName + "</td><td class=TournamentName>" + item.TournamentName + "</td><td class=Points>" + item.Points + "</td></tr>";
                                });
                                myhtml += "</tbody></table>";
                                $('#teamPicks').html(myhtml);
                            })
                        }
                    }).selectmenu("menuWidget")
                    .addClass("overflow");
            })
            .fail(function(data) {
                console.log(JSON.stringify(data));
            });

        $("#autocomplete").autocomplete({
            source: function(request, response) {
                $.getJSON("./js/data/getData.php?f=getPlayers&t=" + thisUser.UserName + "&p=" + request.term + "&_ts=" + Date.now(), function(data) {
                        if (data.length > 0) {
                            var arr = [];
                            $.each(data, function(kk, item) {
                                arr.push(item.FullName);
                            });
                            response(arr);
                        }
                    })
                    .fail(function(data) {
                        console.log(JSON.stringify(data));
                    });
            },
            select: function(event, ui) {
                var test = ui;
            },
            search: function(event, ui) {
                return true;
            },
            minLength: 3,
            open: function(event, ui) { //allowing single click selection in iOS
                $('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
            },
        });

        $("#btnSavePick").click(function(event) {
            if ($('#autocomplete').val().length > 0) {
                $.post("./js/data/saveData.php?f=savePick&n=" + $('body').data('teamName') + "&t=" + $('#spanTournament')[0].innerText + "&p=" + $('#autocomplete').val() + "&_ts=" + Date.now(), function(data) {
                        if (data.length = 1) {
                            location.href = location.href.split('#')[0];
                        } else {
                            $('#autocomplete').val('');
                        }
                    })
                    .fail(function(data) {
                        console.log(JSON.stringify(data));
                    });
            }
        });
        $("#btnRemove").click(function(event) {
            $.post("./js/data/removeData.php?f=removePick&n=" + $('body').data('teamName') + "&t=" + $('#spanTournament')[0].innerText + "&_ts=" + Date.now(), function(data) {
                    if (data.length = 1) {
                        location.href = location.href.split('#')[0]; //+ '&_ts=' + Date.now();
                    } else {
                        $('#autocomplete').val('');
                    }
                })
                .fail(function(data) {
                    console.log(JSON.stringify(data));
                });
        });
        $("#btnSendMessage").click(function(event) {
            link = document.createElement('a');
            var vsendto = "kelly@wheresolutions.com";
            var data = "mailto:" + vsendto +
                "?subject=" + $('#subject').val() + "&body=A message from: " + $('#name').val() + "(" + $('#email').val() + ")\n\n" + $('#message').val();
            data = encodeURI(data);
            link.setAttribute('href', data)
            link.setAttribute('target', '_blank');
            link.click();
        });
        $("#btnEmailTeams").click(function(event) {
            var users = $('body').data("users");
            var bcc = '';
            $.each(users, function(kk, item) {
                bcc += item.Email + ',';
            });
            var data = "mailto:Kelly@wheresolutions.com?bcc=" + bcc + "&subject=mygolfpicks Reminder&body=Time is getting short, better get your picks in!";

            link = document.createElement('a');
            data = encodeURI(data);
            link.setAttribute('href', data)
            link.setAttribute('target', '_blank');
            link.click();
        });

        // get standings 
        $.getJSON("./js/data/getData.php?f=getStandings" + "&_ts=" + Date.now(), function(data) {
                var myhtml = "<table>"
                $.each(data, function(kk, item) {
                    myhtml += "<tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=TeamName>" + item.TeamName + "</td>" + "<td class=TotalPoints>" + item.TotalPoints.toLocaleString('en'); + "</td></tr>";
                });
                myhtml += "</table>";
                $('#pStandings').html(myhtml);
            })
            .fail(function(data) {
                console.log(JSON.stringify(data));
            });

        if ($('body').data('uid') === '4b2f320f-ba20-461e-a1bf-1b61ff8604ac') {
            $('#ADMIN').show();
            var users = $('body').data("users");
            var payments = $('body').data("payments");
            var firstEmail = "";
            var userEmail = [];
            $.each(users, function(kk, item) {
                firstEmail += item.Email + '|' + 'http://mygolfpicks.azurewebsites.net/?' + item.Id + '||';
                var paid = $.grep($('body').data("payments"), function(pitem) {
                    return (pitem.TeamName == item.UserName);
                })[0].Payment;
                paid = (paid === '1') ? "$$$" : "";
                $('#selectmenu').append('<option value="' + item.UserName + '">' + item.UserName + ' (' + item.Email + ') - ' + paid + '</option>');
            });
            $("#selectmenu").selectmenu({
                select: function(event, ui) {
                    var user = $.grep($('body').data("users"), function(item) {
                        return (item.UserName == ui.item.value);
                    })[0];
                    var body = "Use the following URL for this seasons mygolfpicks weekly picks.\n";
                    body += "You can access the site from your desk, phone or tablet, but you must use this exact URL.\nIf you share this URL with anyone, you can expect your picks to change without notice.\n\n";
                    body += "Click the link below or copy and paste it into your browser address bar.​\n"
                    body += '\n' + ' http://mygolfpicks.azurewebsites.net/?' + user.Id + "\n\nCheers,\n\nKelly Ratchinsky - mygolfpicks";
                    var anemail = "mailto:" + user.Email + "?&subject=" + "mygolfpicks.com - " + user.UserName + " pick URL" + "&body=" + body; // todo: add phone email to this
                    data = encodeURI(anemail);
                    link = document.createElement('a');
                    link.setAttribute('href', data)
                    link.setAttribute('target', '_blank');
                    link.click();
                }
            });
        }
    } else {
        $('#divLoading').html('A valid id was not found, bugger off...')
    }
}

function refreshPage() {
    location.href = location.href + '?_ts=' + Date.now() + '#THISWEEKSPICKS';
}

function updatePlayers() {
    $.getJSON("./js/data/getData.php?f=getAllPlayers&_ts=" + Date.now(), function(data) {
            $.each($('body').data('tnStandings'), function(kk, item2) {
                foundPlayer = $.grep(data, function(item) {
                    return item2.PlayerName === item.FullName;
                });
                if (foundPlayer.length === 0) {
                    // add to players table
                    $.post("./js/data/saveData.php?f=savePlayer&p=" + item2.PlayerName + "&_ts=" + Date.now(), function(data) {
                            console.log(item2.PlayerName);
                            return false;
                        })
                        .fail(function(data) {
                            console.log(JSON.stringify(data));
                            return false;
                        });
                }
            });
        })
        .fail(function(data) {
            console.log(JSON.stringify(data));
        });
}

function loadESPN() {
    $('#divESPN').load('http://www.espn.com/golf/leaderboard?_ts=' + Date.now());
    setTimeout(function() {
        var players = $('.full-name'); // array of player Names
        var winnings = $('tr').find('td.officialAmount'); // array of winnings
        var position = $('tr').find('td.position'); // array of winnings
        var today = new Date();
        var lastWeek = new Date();
        lastWeek.setDate(today.getDate() - 6);
        chkTournament = $.grep($('body').data("tournaments"), function(item) {
            return (new Date(item.DtTournament + "T05:00:00Z")) > (lastWeek);
        })[0];

        var tnStandings = [];
        $.each(players, function(kk, item) {
            tnStandings.push({ "PlayerName": item.innerText, "Winnings": winnings[kk].innerText, "Pos": position[kk].innerText.replace(/-/g, 'MC'), "SortOrder": position[kk].innerText.replace(/-/g, 999).replace(/T/g, '') });
        })
        $('body').data('tnStandings', tnStandings);
        var picksInOrder = [];
        // get This Weeks Picks 
        $.getJSON("./js/data/getData.php?f=getPicksThisWeek&t=" + chkTournament.Name + "&_ts=" + Date.now(), function(data) {
                if (data.length > 0) {
                    var myhtml = "<h3>" + chkTournament.Name + "</h3><br><table>"
                    $.each(data, function(kk, item) {
                        var test = item;
                        try {
                            var espnPlayer = $.grep(tnStandings, function(item2) {
                                return item2.PlayerName == item.PlayerName;
                            })[0];
                            picksInOrder.push({ TeamName: item.TeamName, "PlayerName": espnPlayer.PlayerName, "pos": espnPlayer.Pos, "SortOrder": espnPlayer.SortOrder })
                        } catch (e) {
                            picksInOrder.push({ TeamName: item.TeamName, PlayerName: item.PlayerName, "pos": "DNP", "SortOrder": 1000 })
                            pos = 'DNP'
                        }
                    });
                    picksInOrder.sort(function(a, b) {
                        return a.SortOrder - b.SortOrder || a.PlayerName.localeCompare(b.PlayerName);
                    });
                    $.each(picksInOrder, function(kk, item) {
                        myhtml += "<tr class=" + ((kk % 2 == 0) ? "oddRow" : "evenRow") + "><td class=position>" + item.pos + "</td><td class=TeamName>" + item.TeamName + "</td>" + "<td class=PlayerName2>" + item.PlayerName; + "</td></tr>";
                    });
                    myhtml += "</table>";
                    $('#pThisWeeksPicks').html(myhtml);
                } else
                    $('#pThisWeeksPicks').html("You're too early, come back Thursday...");
            })
            .fail(function(data) {
                console.log(JSON.stringify(data));
            });
    }, 2000);
}

function updatePoints() {
    $.getJSON("./js/data/getData.php?f=getPicksThisWeek&t=" + chkTournament.Name + "&_ts=" + Date.now(), function(data) {
            $('body').data('thisweekspicks', data);
            $.each($('body').data('tnStandings'), function(kk, item) {
                var vpicks = $('body').data('thisweekspicks');
                thisPick = $.grep(vpicks, function(pitem) {
                    return pitem.PlayerName === item.PlayerName;
                });
                if (thisPick.length > 0) {
                    var pts = parseInt(item.Winnings.replace('$', '').replace(/,/g, ''));
                    if (pts > 0) {
                        $.each(thisPick, function(kk, item2) {
                            $.post("./js/data/saveData.php?f=savePoints&n=" + item2.TeamName + "&t=" + chkTournament.Name + "&p=" + pts + "&_ts=" + Date.now(), function(data) {
                                    console.log(JSON.stringify(data));
                                })
                                .fail(function(data) {
                                    console.log(JSON.stringify(data));
                                });
                        });
                    }
                }
            });
            alert("Points Updated");
        })
        .fail(function(data) {
            console.log(JSON.stringify(data));
        });
}

$(document).ready(function() {
    initApp();
    loadESPN();
});