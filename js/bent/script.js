//LOADER/SPINNER
$(window).bind("load", function() {

    "use strict";

    $(".spn_hol").fadeOut(1000);
});


//MENU APPEAR AND HIDE
$(document).ready(function() {

    "use strict";
    $(".navbar").css({
        'margin-top': '0px',
        'opacity': '1'
    })
    $(".navbar-nav>li>a").css({
        'padding-top': '15px'
    });
    $(".navbar-brand img").css({
        'height': '35px'
    });
    $(".navbar-brand img").css({
        'padding-top': '0px'
    });
    $(".navbar-default").css({
        'background-color': 'rgba(59, 59, 59, 0.7)'
    });

    // $(window).scroll(function () {

    //     "use strict";

    //     // if ($(window).scrollTop() > 80) {
    //         $(".navbar").css({
    //             'margin-top': '0px',
    //             'opacity': '1'
    //         })
    //         $(".navbar-nav>li>a").css({
    //             'padding-top': '15px'
    //         });
    //         $(".navbar-brand img").css({
    //             'height': '35px'
    //         });
    //         $(".navbar-brand img").css({
    //             'padding-top': '0px'
    //         });
    //         $(".navbar-default").css({
    //             'background-color': 'rgba(59, 59, 59, 0.7)'
    //         });
    // } else {
    //     $(".navbar").css({
    //         'margin-top': '-100px',
    //         'opacity': '0'
    //     })
    //     $(".navbar-nav>li>a").css({
    //         'padding-top': '45px'
    //     });
    //     $(".navbar-brand img").css({
    //         'height': '45px'
    //     });
    //     $(".navbar-brand img").css({
    //         'padding-top': '20px'
    //     });
    //     $(".navbar-default").css({
    //         'background-color': 'rgba(59, 59, 59, 0)'
    //     });
    // }
    // });
});




// MENU SECTION ACTIVE
$(document).ready(function() {

    "use strict";

    $(".navbar-nav li a").click(function() {

        "use strict";

        $(".navbar-nav li a").parent().removeClass("active");
        $(this).parent().addClass("active");
    });
});



// Hilight MENU on SCROLl

$(document).ready(function() {

    "use strict";

    $(window).scroll(function() {

        "use strict";

        $(".page").each(function() {

            "use strict";

            var bb = $(this).attr("id");
            var hei = $(this).outerHeight();
            var grttop = $(this).offset().top - 70;
            if ($(window).scrollTop() > grttop - 1 && $(window).scrollTop() < grttop + hei - 1) {
                var uu = $(".navbar-nav li a[href='#" + bb + "']").parent().addClass("active");
            } else {
                var uu = $(".navbar-nav li a[href='#" + bb + "']").parent().removeClass("active");
            }
        });
    });
});



//SMOOTH MENU SCROOL


$(function() {

    "use strict";

    // $('a[href*=#]:not([href=#])').click(function () {
    //     if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    //         var target = $(this.hash);
    //         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    //         if (target.length) {
    //             $('html,body').animate({
    //                 scrollTop: target.offset().top
    //             }, 1000);
    //             return false;
    //         }
    //     }
    // });
});



// FIX HOME SCREEN HEIGHT
$(document).ready(function() {

    "use strict";

    setInterval(function() {

        "use strict";

        var windowHeight = $(window).height();
        var containerHeight = $(".home-container").height();
        var padTop = windowHeight - containerHeight;
        $(".home-container").css({
            'padding-top': Math.round(padTop / 2) + 'px',
            'padding-bottom': Math.round(padTop / 2) + 'px'
        });
    }, 10)
});



//PARALLAX
// $(document).ready(function() {

//     "use strict";

//     $(window).bind('load', function() {
//         "use strict";
//         parallaxInit();
//     });

//     function parallaxInit() {
//         "use strict";
//         // $('.testimonial').parallax("10%", 1);
//         // $('.home-parallax').parallax("30%", 0.1);
//         // $('.subscribe-parallax').parallax("30%", 0.1);
//         /*add as necessary*/
//     }
// });




//CONTACT FORM VALIDATION
$(document).ready(function() {

    "use strict";

    $(".form_submit").click(function() {

        "use strict";

        var name = $("#name").val();
        var email = $("#email").val();
        var subject = $("#subject").val();
        var message = $("#message").val();
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if (!name) {
            $(".form_error .name_error").addClass("show").removeClass("hide");
            return false;
        } else {
            $(".form_error .name_error").addClass("hide").removeClass("show");
        }
        if (!email) {
            $(".form_error .email_error").addClass("show").removeClass("hide");
            return false;
        } else {
            $(".form_error .email_error").addClass("hide").removeClass("show");
            if (testEmail.test(email)) {
                $(".form_error .email_val_error").addClass("hide").removeClass("show");
            } else {
                $(".form_error .email_val_error").addClass("show").removeClass("hide");
                return false;
            }
        }
        if (!message) {
            $(".form_error .message_error").addClass("show").removeClass("hide");
            return false;
        } else {
            $(".form_error .message_error").addClass("hide").removeClass("show");
        }
        if (name && email && message) {
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: "./emailHandler.ashx",
                data: 'name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message,
                dataType: "json",
                success: function(response) {
                    //console.log(response.url);
                },
                error: function(message) {
                    //alert("error has occured");
                    console.log(message.statusText);
                }
            });


            //$.ajax({
            //    url: 'contact.php',
            //    data: {
            //        name: name,
            //        emaild: emaild,
            //        subject: subject,
            //        message: message
            //    },
            //    type: 'POST',
            //    success: function(data) {
            //        $(".Sucess").show();
            //        $(".Sucess").fadeIn(2000);
            //        $(".Sucess").html("<i class='fa fa-check'></i> Dear <b>" + name + "</b> Thank you for your inquiry we will respond to you as soon as possible!");
            //        $("#Name").val("");
            //        $("#Email").val("");
            //        $("#Subject").val("");
            //        $("#Message").val("");
            //        $(".form_error .name_error, .form_error .email_error, .form_error .email_val_error, .form_error .message_error").addClass("hide").removeClass("show");
            //        $("#name").val("");
            //        $("#email").val("");
            //        $("#subject").val("");
            //        $("#message").val("");
            //    }
            //});
        }
        return false;
    });
});



/// SMOOTH SCROLL           

$(document).ready(function() {

    "use strict";

    var scrollAnimationTime = 1200,
        scrollAnimation = 'easeInOutExpo';
    $('a.scrollto').bind('click.smoothscroll', function(event) {
        event.preventDefault();
        var target = this.hash;
        $('html, body').stop().animate({
            'scrollTop': $(target).offset().top
        }, scrollAnimationTime, scrollAnimation, function() {
            window.location.hash = target;
        });
    });
    //COUNTER
    // $('.counter_num').counterUp({
    //     delay: 10,
    //     time: 2000
    // });
});