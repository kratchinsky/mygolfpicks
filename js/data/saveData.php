<?php

if ($_GET['f'] === "savePick") {
    savePick();
} elseif ($_GET['f'] === "savePoints") {
    savePoints();
} elseif ($_GET['f'] === "savePlayer") {
    savePlayer();
// } elseif ($_GET['f'] === "getPlayers") {
//     getPlayers();
// } elseif ($_GET['f'] === "getPicksThisWeek") {
//     getPicksThisWeek();
// } elseif ($_GET['f'] === "getStandings") {
//     getStandings();
}
function savePoints() {
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tournamentName = $_GET['t'];
        $points = $_GET['p'];
        $teamName = $_GET['n'];
        $tsql = "UPDATE picks set Points = " . $points . " where TeamName = :n and TournamentName = :t";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':t', $tournamentName);
        $statement->bindValue(':n', $teamName);

        $conn->beginTransaction();
        $statement->execute();
        $conn->commit();
        $Kount = $statement->rowCount();
        echo "Pick Saved, TournamentName: " . $tournamentName . ", Points: " . $points . ", TeamName: " . $teamName . ", rowCount: " . $Kount;
    }  
   catch(Exception $e)  
    {  
        echo $e;  
    }  
     finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function savePick(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tournamentName = $_GET['t'];
        $playerrName = $_GET['p'];
        $teamName = $_GET['n'];
        $tsql = "INSERT into picks values (0, :n, :p, :t)";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':t', $tournamentName);
        $statement->bindValue(':p', $playerrName);
        $statement->bindValue(':n', $teamName);

        $conn->beginTransaction();
        $statement->execute();
        $conn->commit();
        $Kount = $statement->rowCount();


        echo "Pick Saved, TournamentName: " . $tournamentName . ", PlayerName: " . $playerrName . ", TeamName: " . $teamName . ", rowCount: " . $Kount;
    }  
   catch(Exception $e)  
    {  
        echo $e;  
    }  
     finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function savePlayer(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $playerrName = $_GET['p'];
        $tsql = "INSERT into players values (:p)";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':p', $playerrName);

        $conn->beginTransaction();
        $statement->execute();
        $conn->commit();
        $Kount = $statement->rowCount();

        echo "Player Saved";
    }  
   catch(Exception $e)  
    {  
        echo $e;  
    }  
     finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

?>