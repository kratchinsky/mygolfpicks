﻿<?php

if ($_GET['f'] === "getUsers") {
    getUsers();
} elseif ($_GET['f'] === "getTournaments") {
    getTournaments();
} elseif ($_GET['f'] === "getPicks") {
    getPicks();
} elseif ($_GET['f'] === "getPlayers") {
    getPlayers();
} elseif ($_GET['f'] === "getAllPlayers") {
    getAllPlayers();
} elseif ($_GET['f'] === "getPicksThisWeek") {
    getPicksThisWeek();
} elseif ($_GET['f'] === "getStandings") {
    getStandings();
} elseif ($_GET['f'] === "getPayments") {
    getPayments();
}    

function getUsers(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tsql = "SELECT * FROM users";
        $statement=$conn->prepare($tsql);
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}
function getPayments(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tsql = "SELECT * FROM Payments";
        $statement=$conn->prepare($tsql);
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function getTournaments(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tsql = "SELECT * FROM tournaments order by SortId";
        $statement=$conn->prepare($tsql);
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function getPicks(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $teamName = $_GET['t'];
        // echo $teamName;
        $tsql = "SELECT * FROM picks where TeamName = :t;";

        $statement=$conn->prepare($tsql);
        $statement->bindValue(':t', $teamName);
        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function getAllPlayers(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tsql = "SELECT fullname from players";
        $statement=$conn->prepare($tsql);

        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
   }
}

function getPlayers(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $playerName = "%".$_GET['p']."%";
        $teamName = $_GET['t'];
        $tsql = "SELECT fullname from players where fullname not in (select playername from picks where teamname = :t) and fullname like :p";
        // $tsql = "SELECT FullName FROM players where FullName like :p";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':p', $playerName);
        $statement->bindValue(':t', $teamName);

        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
   }
}

function getPicksThisWeek(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tournamentName = $_GET['t'];
        $tsql = "SELECT * from picks where TournamentName = :t order by PlayerName";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':t', $tournamentName);

        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

function getStandings(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tsql = "SELECT TeamName, sum(points) TotalPoints from picks group by teamname order by sum(points) desc";
        $statement=$conn->prepare($tsql);

        $statement->execute();
        $results=$statement->fetchAll(PDO::FETCH_ASSOC);
        $json=json_encode($results);

        echo $json;
    }  
    catch(Exception $e)  
    {  
        echo "Error!";  
    }  
    finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

?>