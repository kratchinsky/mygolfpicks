<?php

if ($_GET['f'] === "removePick") {
    removePick();
// } elseif ($_GET['f'] === "getTournaments") {
//     getTournaments();
// } elseif ($_GET['f'] === "getPicks") {
//     getPicks();
// } elseif ($_GET['f'] === "getPlayers") {
//     getPlayers();
// } elseif ($_GET['f'] === "getPicksThisWeek") {
//     getPicksThisWeek();
// } elseif ($_GET['f'] === "getStandings") {
//     getStandings();
}

function removePick(){
    try  
    {  
        $conn =  new PDO("sqlite:mygolfpicks.db");
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if( $conn === false ) {
            echo "Unable to connect.</br>";
        }
        /* TSQL Query */
        $tournamentName = $_GET['t'];
        $teamName = $_GET['n'];
        $tsql = "DELETE from picks WHERE TournamentName = :t AND TeamName = :n";
        $statement=$conn->prepare($tsql);
        $statement->bindValue(':t', $tournamentName);
        $statement->bindValue(':n', $teamName);

        $conn->beginTransaction();
        $statement->execute();
        $conn->commit();
        $Kount = $statement->rowCount();

        echo "Pick Saved, TournamentName: " . $tournamentName . ", TeamName: " . $teamName . ", rowCount: " . $Kount;
    }  
   catch(Exception $e)  
    {  
        echo $e;  
    }  
     finally {
        unset($conn);
        unset($statement);
        $conn=null;
        $statement=null;
    }
}

?>